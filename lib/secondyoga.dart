import 'package:apitask/thirdyoga.dart';
import 'package:flutter/material.dart';

class SecondYoga extends StatefulWidget {
  dynamic name;
  dynamic user;
  dynamic image;

  SecondYoga({Key? key, this.name, this.user, this.image}) : super(key: key);

  @override
  State<SecondYoga> createState() => _SecondYogaState();
}

class _SecondYogaState extends State<SecondYoga> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(209, 208, 207, 0.9),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            children: [
              Container(
                height: 250,
                // color: Colors.amber,
                child: Row(
                  children: [
                    Center(
                      child: Container(
                        height: 500,
                        // color: Colors.amber,
                        padding: EdgeInsets.all(20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            InkWell(
                              onTap: () {
                                Navigator.pop(context);
                              },
                              child: Container(
                                width: 50,
                                height: 50,
                                decoration: BoxDecoration(
                                  // color: Colors.white,
                                  borderRadius: BorderRadius.circular(10),
                                  border: Border.all(),
                                ),
                                padding: EdgeInsets.all(10),
                                child: Center(
                                  child: Icon(
                                    Icons.arrow_back_ios,
                                    color: Colors.black,
                                    size: 15,
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            Container(
                              width: 250,
                              // color: Colors.amber,
                              child: Text(
                                overflow: TextOverflow.ellipsis,
                                widget.name.toString() + '\nClub',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 30),
                              ),
                            ),
                            SizedBox(
                              height: 30,
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  left: 15, bottom: 7, top: 7, right: 15),
                              decoration: BoxDecoration(
                                color: Color.fromRGBO(150, 100, 50, 0.9),
                                borderRadius: BorderRadius.circular(10),
                              ),
                              child: Text(
                                widget.user.toString() + ' members',
                                style: TextStyle(
                                  fontWeight: FontWeight.w300,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Center(
                      child: Container(
                        height: 500,
                        // padding: EdgeInsets.only(right: 100),
                        // margin: EdgeInsets.only(right: 100),
                        width: 100,
                        // color: Colors.amber,
                        child: Image.network(widget.image , fit: BoxFit.contain,),
                        // color: Colors.deepOrange,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  color: Color.fromRGBO(255, 255, 255, 0.9),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(45),
                    topRight: Radius.circular(45),
                  ),
                ),
                height: 630,
                child: ListView(
                  physics: BouncingScrollPhysics(),
                  children: [
                    Cardgenerate(
                        'assets/images/treepose_pexels5.png',
                        '10 days',
                        'Tree Pose',
                        Color.fromRGBO(209, 208, 207, 0.6),
                        300.0,
                        500.0,
                        'Simultaneously, slide your left arm down along your left leg till your fingers are at your ankle.',
                        'Bend one leg at the knee. Choose the leg you are going to fold in first. ',
                        padding: 10.0),
                    Cardgenerate(
                        'assets/images/triangle_pexels6.png',
                        '12 days',
                        'Triangle Pose',
                        Color.fromRGBO(152, 175, 199, 0.6),
                        270.0,
                        500.0,
                        'Lengthen through both sides of the waist, draw your lower belly in and up and lift your arms parallel to the floor. ',
                        'Point the left arm straight up to the ceiling, hand in line with your shoulder, palm facing forwards ',
                        padding: 200.0),
                    Cardgenerate(
                        'assets/images/bakasana_pexels7.png',
                        '12 days',
                        'Bakasana',
                        Color.fromRGBO(130, 120, 100, 0.5),
                        300.0,
                        600.0,
                        'Slide your triceps down your shins so that the halfway point of the triceps aligns with the halfway point of the shins.',
                        'Ensure that the pose is compact from the sides towards the centre, from the front towards the back.',
                        padding: 60.0),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget Cardgenerate(
      image, names, name, color, width, height, discription1, discription2,
      {padding}) {
    return InkWell(
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) {
              return ThirdYoga(
                name: name,
                image: image,
                discription1: discription1,
                discription2: discription2,
              );
            },
          ),
        );
      },
      child: Container(
        height: 200,
        margin: EdgeInsets.only(
          top: 30,
          left: 20,
          right: 20,
        ),
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(30),
          // color: Colors.lightGreen
        ),
        child: Stack(
          alignment: AlignmentDirectional.bottomEnd,
          children: [
            Container(
              // width: width,
              // height: height,
              // color: Colors.amber,
              padding: EdgeInsets.only(left: padding ?? 30, bottom: 1),
              child: Container(
                width: 200,
                height: 400,
                // color: Colors.amber,
                child: Image.asset(image, fit: BoxFit.fitHeight),
              ),
            ),
            Positioned(
              left: 0,
              child: Container(
                // width: 180,
                // height: 260,
                // color: Colors.white,
                padding: EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.only(
                        right: 10,
                        top: 5,
                        bottom: 5,
                        left: 10,
                      ),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Text(
                        names,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      name,
                      style: TextStyle(
                        fontSize: 25,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      // width: 400,
                      // color: Colors.amber,
                      height: 60,
                      child: Row(
                        children: [
                          Icon(Icons.access_time),
                          Text('20 min'),
                          SizedBox(
                            width: 10,
                          ),
                          Icon(Icons.calendar_month),
                          Text('1 - 31 march'),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
