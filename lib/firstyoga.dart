import 'dart:convert';

import 'package:apitask/insertdata.dart';
import 'package:apitask/secondyoga.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class FirstYoga extends StatefulWidget {
  const FirstYoga({Key? key}) : super(key: key);

  @override
  State<FirstYoga> createState() => _FirstYogaState();
}

class _FirstYogaState extends State<FirstYoga> {
  @override
  void initState() {
    getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        color: Colors.black,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      padding: EdgeInsets.all(10),
                      child: Center(
                        child: Icon(
                          Icons.menu,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(),
                          borderRadius: BorderRadius.circular(15)),
                      padding: EdgeInsets.all(10),
                      child: Center(
                          child: Icon(Icons.search, color: Colors.black)),
                    ),
                  ],
                ),
                SizedBox(
                  height: 25,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Fitness Club',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 30,
                        fontWeight: FontWeight.w900,
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                          return InsertData(map: null,);
                        },),).then((value) {
                          setState(() {

                          });
                        },);
                      },
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(),
                            borderRadius: BorderRadius.circular(15)),
                        padding: EdgeInsets.all(10),
                        child: Center(
                            child: Icon(Icons.add, color: Colors.black)),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 25,
                ),
                Container(
                  height: 270,
                  // color: Colors.amber,
                  child: FutureBuilder<http.Response>(
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        dynamic jsonData =
                            jsonDecode(snapshot.data!.body.toString());
                        return ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: jsonData.length,
                          itemBuilder: (context, index) {
                            return InkWell(
                              onTap: () {
                                Navigator.of(context).push(
                                  MaterialPageRoute(
                                    builder: (context) {
                                      return SecondYoga(
                                        name: jsonData[index]["name"],
                                        user: "5,634",
                                        image: jsonData[index]["avatar"],
                                      );
                                    },
                                  ),
                                );
                              },
                              child: Container(
                                height: 270,
                                margin: EdgeInsets.only(right: 15),
                                decoration: BoxDecoration(
                                  color: Color.fromRGBO(209, 208, 207, 0.6),
                                  borderRadius: BorderRadius.circular(30),
                                  // color: Colors.lightGreen
                                ),
                                child: Stack(
                                  alignment: AlignmentDirectional.bottomEnd,
                                  children: [
                                    Container(
                                      width: 210,
                                      height: 300,
                                      // color: Colors.amber,
                                      padding:
                                          EdgeInsets.only(left: 30, bottom: 20),
                                      child: Image.network(
                                          jsonData[index]["avatar"],
                                          fit: BoxFit.contain),
                                    ),
                                    Positioned(
                                      top: 0,
                                      left: 0,
                                      child: Container(
                                        width: 210,
                                        height: 282,
                                        // color: Colors.white,
                                        padding: EdgeInsets.all(20),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Container(
                                                  width: 130,
                                                  // color: Colors.amber,
                                                  child: Text(
                                                    jsonData[index]["name"],
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    style: TextStyle(
                                                      fontSize: 25,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    ),
                                                  ),
                                                ),
                                                Text(
                                                  "Club",
                                                  style: TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 25,
                                                  ),
                                                ),
                                                SizedBox(
                                                  height: 10,
                                                ),
                                                Container(
                                                  child: Row(
                                                    children: [
                                                      Icon(Icons.man,
                                                          color: Colors.black45,
                                                          size: 18),
                                                      Text(
                                                        "5,634",
                                                        style: TextStyle(
                                                          color: Colors.black45,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                SizedBox(
                                                  height: 50,
                                                ),
                                                Container(
                                                  padding: EdgeInsets.only(
                                                      top: 5,
                                                      right: 10,
                                                      bottom: 5,
                                                      left: 10),
                                                  decoration: BoxDecoration(
                                                    color: Colors.white,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10),
                                                  ),
                                                  child: Text("join"),
                                                )
                                              ],
                                            ),
                                            Column(
                                              children: [
                                                Container(
                                                  width: 35,
                                                  height: 35,
                                                  decoration: BoxDecoration(
                                                    color: Colors.grey,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10),
                                                  ),
                                                  child: InkWell(
                                                    onTap: () {
                                                      Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                                                        return InsertData(map: jsonData[index],);
                                                      },),).then((value) {
                                                        setState(() {

                                                        });
                                                      },);
                                                    },
                                                    child: Icon(Icons.edit , color: Colors.white,),
                                                  ),
                                                ),
                                                SizedBox(
                                                  height: 10,
                                                ),
                                                InkWell(
                                                  onTap: () {
                                                    showAlertDialog(context, jsonData[index]["id"]);
                                                  },
                                                  child: Container(
                                                    width: 35,
                                                    height: 35,
                                                    decoration: BoxDecoration(
                                                      color: Colors.grey,
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              10),
                                                    ),
                                                    child: Icon(Icons.delete , color: Colors.red),
                                                  ),
                                                ),
                                              ],
                                            )
                                          ],
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            );
                          },
                        );
                      } else {
                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      }
                    },
                    future: getData(),
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                Text(
                  'Daily',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 30,
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                Container(
                  height: 350,
                  child: ListView(
                    // scrollDirection: Axis.horizontal,
                    physics: BouncingScrollPhysics(),
                    children: [
                      Listmaker(
                        Color.fromRGBO(203, 136, 200, 0.66),
                        Icons.do_not_step,
                        Colors.white,
                        'Steps',
                        'Goal 8000/day',
                        '2343',
                      ),
                      Listmaker(
                          Color.fromRGBO(233, 116, 81, 0.6),
                          Icons.local_fire_department_sharp,
                          Colors.white,
                          'Calorie',
                          'Weekly Average',
                          '1654'),
                      Listmaker(
                        Color.fromRGBO(85, 93, 101, 0.5),
                        Icons.sports_gymnastics_sharp,
                        Colors.white,
                        'Workout',
                        'Daily time',
                        '45 min',
                      ),
                      Listmaker(
                        Color.fromRGBO(80, 200, 120, 0.6),
                        Icons.access_time,
                        Colors.white,
                        'Time',
                        'Daily',
                        '60 min',
                      ),
                      Listmaker(
                        Color.fromRGBO(0, 255, 255, 0.46),
                        Icons.restaurant,
                        Colors.white,
                        'Food',
                        'Daily diet',
                        '2343',
                      ),
                      Listmaker(
                        Color.fromRGBO(160, 32, 240, 0.51),
                        Icons.account_balance,
                        Colors.white,
                        'Money',
                        '30 Day',
                        '1500',
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget Cardgenerate(image, name, users, color, width, height, {padding}) {
    return InkWell(
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) {
              return Container();
            },
          ),
        );
      },
      child: Container(
        height: 270,
        margin: EdgeInsets.only(right: 15),
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(30),
          // color: Colors.lightGreen
        ),
        child: Stack(
          alignment: AlignmentDirectional.bottomEnd,
          children: [
            Container(
              width: width,
              height: height,
              // color: Colors.amber,
              padding: EdgeInsets.only(left: padding ?? 30, bottom: 1),
              child: Image.network(image, fit: BoxFit.contain),
            ),
            Positioned(
              left: 0,
              child: Container(
                width: 180,
                height: 260,
                // color: Colors.white,
                padding: EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      name,
                      style: TextStyle(
                        fontSize: 25,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      "Club",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 25,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      child: Row(
                        children: [
                          Icon(Icons.man, color: Colors.black45, size: 18),
                          Text(
                            users,
                            style: TextStyle(
                              color: Colors.black45,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 90,
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          top: 5, right: 10, bottom: 5, left: 10),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Text("join"),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget Listmaker(iconbgcolor, icon, iconcolor, title, subtitle, numbers) {
    return ListTile(
      leading: Container(
        padding: EdgeInsets.all(14),
        decoration: BoxDecoration(
            color: iconbgcolor, borderRadius: BorderRadius.circular(10)),
        child: Icon(icon, color: iconcolor),
      ),
      title: Text(
        title,
        style: TextStyle(
          fontWeight: FontWeight.bold,
        ),
      ),
      subtitle: Text(subtitle),
      trailing: Container(
        width: 100,
        padding: EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10),
        decoration: BoxDecoration(
            color: Color.fromRGBO(0, 0, 0, 0.1),
            borderRadius: BorderRadius.circular(15)),
        child: Center(
          child: Text(
            numbers,
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
        ),
      ),
    );
  }

  Future<http.Response> getData() async {
    print("Hello");
    http.Response res = await http.get(
        Uri.parse('https://63fa053cbeec322c57ebfd13.mockapi.io/yogaClubs'));
    print(res.body);
    print("hello");

    return res;
  }

  Future<void> deletePrinter(id) async {
    http.Response res = await http.delete(
        Uri.parse('https://63fa053cbeec322c57ebfd13.mockapi.io/yogaClubs/$id'));
  }

  showAlertDialog(BuildContext context , jsonData) {

    // set up the buttons
    Widget cancelButton = TextButton(
      child: Text("Cancel"),
      onPressed:  () {
        Navigator.of(context).pop();
      },
    );
    Widget continueButton = TextButton(
      child: Text("Continue"),
      onPressed:  () {
        deletePrinter(jsonData).then((value) {
          Navigator.of(context).pop();
          setState(() {

          });
        },);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Alert"),
      content: Text("Are You Sure Want Delete ?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
